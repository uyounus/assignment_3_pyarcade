import curses
import sys
import MainMenu
import MenuBlackjack
import mastermind
import minecraft as minesweeper
import MenuMastermind
import MenuMinesweeper
#sys.path.append('C:\\Users\\Joon\\Desktop\\cmsc435\\assignment3_pyarcade_test_suite')


def main():

    screen = curses.initscr()
    # need these to be menu classes
    mainmenu = MainMenu.MainMenu(["Blackjack","Mastermind", "Minesweeper", "Exit"], 
                                    [MenuBlackjack.MenuBlackjack(), MenuMastermind.MenuMastermind(), MenuMinesweeper.MenuMinesweeper(), "exit"], "Main menu")
     # important wrapper that sets init values for curses like keypad input and cbreak
    curses.wrapper(mainmenu.run)
    


if __name__ == "__main__":
    main()
