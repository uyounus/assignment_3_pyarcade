from minecraft import Minecraft
import unittest


class MinecraftTestCase(unittest.TestCase):
    def test_play_game(self):
        game = Minecraft()
        selection = game.play_game_again()
        # The function returns True if "y" is pressed and 'False' if 'n' is
        # pressed
        if selection == True:
            # doing this to automate the test. Otherwise we can check manually
            # as well
            variable = True
        else:
            variable = False
        self.assertEqual(selection, variable)

    def test_random_position(self):
        size_of_grid = 9
        game = Minecraft(size_of_grid, 10)
        position = game.get_random_position()
        x = position[0]
        y = position[1]
        self.assertEqual(True, x in range(size_of_grid))
        self.assertEqual(True, y in range(size_of_grid))

    def test_neighbor_locations(self):
        # checks the six immediate neighbors of a digit on all sides except
        # for the border cases
        game = Minecraft(9, 10)
        ro = 4
        col = 4
        neighbors = [(ro-1, col-1), (ro-1, col), (ro-1, col+1), (ro, col-1), (ro, col+1), (ro+1, col-1), (ro+1, col), (ro+1, col+1)]

        neighbor_positions = game.neighbors_location(ro, col)
        self.assertEqual(neighbor_positions, neighbors)
#        print(neighbor_positions)

    def test_get_mines(self):
        # checks that the mines are generated in the quanity specified and
        # each number is in range of 0 - 9
        game = Minecraft(9, 10)
        start = (1, 2)
        mines = game.formulate_mines(start)

#        comparison_string = [True]*(2*game.size_of_grid)
        comparison_string = True
        for elements in mines:
            if elements[0] in range(game.size_of_grid) and elements[1] in range(game.size_of_grid):
                select = True
            else:
                select = False
#        self.assertEqual(comparison_string, [True for x in seq if len(seq) > 1 for y in x if y != 'e'])
        self.assertEqual(comparison_string, select)
#        print(mines)

    def test_get_number(self):
        """
            The get number returns a grid of size (axa) as per the grid that is
            passed to it and replaces each position in grid with the number of
            mines in 8 neighbors
        """
        size_of_grid = 8
        game = Minecraft(size_of_grid)

        blankgrid = [['0' for i in range(game.size_of_grid)] for i in range(game.size_of_grid)]
        result = game.get_numbr(blankgrid)
#        print(result)

        self.assertEqual(len(result), game.size_of_grid)
        # Checking if the length of grid is exactly the same as size of grid
        self.assertEqual(result, blankgrid)
        # Checking the output. Since the grid passed to function in blank (0)
        # it should replace all the elements in grid as zero

    def test_display_cells(self):
        # This functions should return a None type value if the grid passed
        # to it is a blankgrid
        row = 2
        column = 2
        grid = []
        game = Minecraft(9, 10)

        blankgrid = [['0' for i in range(game.size_of_grid)] for i in range(game.size_of_grid)]

        returned = game.display_cells(grid, blankgrid, row, column)
        self.assertEqual(returned, None)


if __name__ == "__main__":
    sample = MinecraftTestCase()
    sample.test_play_game()
    sample.test_random_position()
    sample.test_neighbor_locations()
    sample.test_get_mines()
    sample.test_get_number()
    sample.test_display_cells()
