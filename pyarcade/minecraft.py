# Major logic and script for the game is taken from the Mohd Akram; as the game
# display framework was difficult to design
from string import ascii_lowercase
from input_system import MinecraftInput
>>>>>>> pyarcade/minecraft.py
from typing import Optional
import random
import curses
import time
from input_system import MinecraftInput


class Minecraft:
    """

        A Class representing the Minecraft game

    """
    def __init__(self, size_of_grid: Optional[int] = 9, number_of_mine: Optional[int] = 10):
        self.size_of_grid = size_of_grid
        self.mine_number = number_of_mine

    def setup_all_grid(self, start):
        blankgrid = [['0' for i in range(self.size_of_grid)] for i in range(self.size_of_grid)]

        all_mines = self.formulate_mines(blankgrid, start)

        for i, j in all_mines:
            blankgrid[i][j] = 'X'

        overall_grid = self.get_numbr(blankgrid)

        return (overall_grid, all_mines)

    def grid_display(self, overall_grid, stdscr):
        gridsize = len(overall_grid)

        horizontal = '   ' + (4 * gridsize * '-') + '-'

        # Print top column letters
        toplabel = '     '

        for i in ascii_lowercase[:gridsize]:
            toplabel = toplabel + i + '   '

#        print(toplabel + '\n' + horizontal)
        stdscr.addstr(0, 0, (toplabel + '\n' + horizontal))

        # Print left row numbers
        for idx, i in enumerate(overall_grid):
            row = '{0:2} |'.format(idx + 1)

            for j in i:
                row = row + ' ' + j + ' |'

            stdscr.addstr(idx + 1, 0, (row + '\n' + horizontal))
            last = idx

#        print('')
        stdscr.addstr(last + 1, 0, '')
        stdscr.refresh()

    def get_random_position(self, grid):
        size_of_grid = len(grid)
        a = random.randint(0, size_of_grid - 1)
        b = random.randint(0, size_of_grid - 1)

        return (a, b)

    def neighbors_location(self, grid, row_number, column_number):
        size_of_grid = len(grid)
        immed_neighbors = []

        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == 0 and j == 0:
                    continue
                elif -1 < (row_number + i) < size_of_grid and -1 < (column_number + j) < size_of_grid:
                    immed_neighbors.append((row_number + i, column_number + j))

        return immed_neighbors

    def formulate_mines(self, grid, start):
        all_mine = []
        immed_neighbors = self.neighbors_location(grid, *start)

        for i in range(self.mine_number):
            position = self.get_random_position(grid)
            while position == start or position in all_mine or position in immed_neighbors:
                position = self.get_random_position(grid)
            all_mine.append(position)

        return all_mine

    def get_numbr(self, grid):
        values = []
        for row_number, row in enumerate(grid):
            for column_number, cell in enumerate(row):
                if cell != 'X':
                    # Get value of neighbor
                    values = [grid[ro][col] for ro, col in self.neighbors_location(grid, row_number, column_number)]

                    # This will count all the mines
                    grid[row_number][column_number] = str(values.count('X'))
#        print("values", values)
        return grid

    def display_cells(self, grid, current_grid, row_number, column_number):
        # If cell already displated, it is exit function
        if current_grid[row_number][column_number] != ' ':
            return

        # Show current cell
        current_grid[row_number][column_number] = grid[row_number][column_number]

        # If cell empty, get the neighbor
        if grid[row_number][column_number] == '0':
            for row, column in self.neighbors_location(grid, row_number, column_number):
                # Repeat function for non-flag neighbor
                if current_grid[row][column] != 'F':
                    self.display_cells(grid, current_grid, row, column)

    def play_game_again(self, stdscr):
        height, width = stdscr.getmaxyx()

        stdscr.addstr(height//2 + 5, width//2, "Play again? (y/n):")
        stdscr.refresh()

        selection = stdscr.getstr(height//2 + 6, width//2).decode(encoding="utf-8")
#        selection = input('Play again? (y/n): ')
        stdscr.clear()
        return selection.lower() == 'y'

    def game_play(self):
        # Initializing curses screen over here
        stdscr = curses.initscr()
        stdscr.clear()
        height, width = stdscr.getmaxyx()

        gridsize = self.size_of_grid
        numberofmines = self.mine_number

        current_grid = [[' ' for i in range(gridsize)] for i in range(gridsize)]

        grid = []
        flags = []
        starttime = 0

        helpmessage = ("Enter Column followed by row (eg. a5). "
                       "To put or remove a flag, add 'f' to the cell (eg. a5f)."
                       "Enter exit to Quit Game")

        self.grid_display(current_grid, stdscr)

#        print(helpmessage + " Enter 'help' to show message again.\n")
        stdscr.addstr(height//2, 1, helpmessage + " Enter 'help' to show message again.\n")
        stdscr.refresh()

        handler = MinecraftInput()

        while True:
            minesleft = numberofmines - len(flags)
            stdscr.addstr(height//2 + 3, 2, "Enter the cell ({} mines left):".format(minesleft))
            stdscr.refresh()

#            prompt = input('Enter the cell ({} mines left): '.format(minesleft))
            prompt = stdscr.getstr(height//2 + 4, 2).decode(encoding="utf-8")
#            prompt = prompt.format(minesleft)
            result = handler.minecraft_input_handle(prompt, gridsize, helpmessage + '\n')

            message = result['message']
            position = result['cell']
          
            if message == 'exit':
                break

            if message == 'exit':
                print("Exiting the Game.\n")
                break

            if position:
                # print('\n\n')
                stdscr.addstr(height//2 + 4, 1, "\n\n")
                stdscr.refresh()
              
                row_number, column_number = position
                current_cell = current_grid[row_number][column_number]
                flag = result['flag']

                if not grid:
                    grid, mines = self.setup_all_grid(position)
                if not starttime:
                    starttime = time.time()

                if flag:
                    # Add a flag if the cell is empty
                    if current_cell == ' ':
                        current_grid[row_number][column_number] = 'F'
                        flags.append(position)
                    # Remove the flag if there is one
                    elif current_cell == 'F':
                        current_grid[row_number][column_number] = ' '
                        flags.remove(position)
                    else:
                        message = 'Cannot put a flag there'

                # If there is a flag there, show a message
                elif position in flags:
                    message = 'There is a flag there'

                elif grid[row_number][column_number] == 'X':
                    # print('Game Over\n')
                    stdscr.addstr(height//2 + 4, width//2, "Game Over")
                    stdscr.refresh()

                    self.display_cells(grid, current_grid, row_number, column_number)
                    if self.play_game_again(stdscr):
                        curses.endwin()
                        self.game_play()
                      
                    return

                elif current_cell == ' ':
                    self.display_cells(grid, current_grid, row_number, column_number)

                else:
                    message = "That cell is already shown"

                if set(flags) == set(mines):
                    minutes, seconds = divmod(int(time.time() - starttime), 60)
                   
#                    print(
#                        'You Win. '
#                        'It took you {} minutes and {} seconds.\n'.format(minutes,seconds))
                    stdscr.addstr(height//2 + 7, width//2, 'You Win. It took you {} minutes and {} seconds.\n'.format(minutes,seconds))
                    stdscr.refresh()

                    self.grid_display(grid, stdscr)
                    if self.play_game_again(stdscr):
                        curses.endwin()
                        self.game_play()
                    return

            self.grid_display(current_grid, stdscr)
#            print(message)
#            stdscr.addstr(12, 1, str(message))
            stdscr.refresh()
            curses.endwin()


#if __name__ == "__main__":
#    game = Minecraft(9, 10)
#    game.game_play()
