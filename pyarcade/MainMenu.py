import curses


class MainMenu:

    """
    menu takes a list of menu options which can hold game menu constructors so we can add more games as we go
    based on the position in the options list the user selects we can start up a constructor at that specified position
    this way changes to the list passed into the constructor will effectively allow for changes to our menu
    """

    def __init__(self, display_options: list, options: list, menu_type: str):
        self.display_options = display_options
        self.options = options
        self.menu_type = menu_type
        self.stdscr1 = curses.initscr()
    
    def set_options(self, options: list):
        self.options = options

    def set_display_options(self, display_options: list):
        self.display_options = display_options

    def set_menu_type(self, menu_type: str):
        self.menu_type = menu_type
        
    def print_menu(self, stdscr, selected_row_idx):
        stdscr.clear()
        h, w = stdscr.getmaxyx()

        # printing menu in the middle of screen by getting max x y co-ord and iterating vertically
        for idx, row in enumerate(self.display_options):
            x = w//2 - len(row)//2
            y = h//2 - len(self.display_options)//2 + idx
            if idx == selected_row_idx:
                stdscr.attron(curses.color_pair(1))
                stdscr.addstr(y, x, row)
                stdscr.attroff(curses.color_pair(1))
            else:
                stdscr.addstr(y, x, row)
        stdscr.refresh()
    
    def select_option(self, stdscr, current_row):
        while True:
            key = stdscr.getch()
 
            if key == curses.KEY_UP and current_row > 0:
                current_row -= 1
            elif key == curses.KEY_DOWN and current_row < len(self.display_options)-1:
                current_row += 1
            elif key == curses.KEY_ENTER or key in [10, 13]:
                
                if current_row == len(self.display_options)-1:
                    break
                
                self.options[current_row].run(self.stdscr1)
                
                # if user selected last row, exit the program
                
            self.print_menu(stdscr, current_row)

    def run(self, stdscr):
        curses.start_color()
        # turn off cursor blinking
        curses.curs_set(0)

        # color scheme for selected row
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)

        # specify the current selected row
        current_row = 0

        # print the menu
        self.print_menu(stdscr, current_row)
        self.select_option(stdscr, current_row)
        return
        

