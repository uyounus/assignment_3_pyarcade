from typing import Optional
from string import ascii_lowercase
import re
# from mastermind import Mastermind


class MastermindInput:
    """ Takes input from the user and uses the defined functions function to
    check if the input is correct
    """
    def __init__(self, handler_width: Optional[int] = 4):
        self.handler_width = handler_width

    def handle_input(self, string: str, master: object):
        """
            Checking if the input is all digits or alphabets for correctness
            and to check for the clear and reset states
        """
        if string.isdigit():
            if len(string) == self.handler_width:
                return True
            else:
                print("Your guessed Number length is not correct. Try again")
                return False
        elif string.isalpha():
            if string == 'exit':
                return True
            elif string == 'clear' or string == 'reset':
                self.memory_clear(string, master)
                return False

    def memory_clear(self, command: str, object) -> None:
        """
            Manages the overall memory of the users and individual game memory

            in case of the clear and reset commands.
        """
        if command == 'clear':
            # clear the Entire History of the game
            object.master_game_data = list([])
            object.attempts = 0
        if command == 'reset':
            # clear the current game data and functional variables
            object.attempts = 0
            object.master_game_data = list([])
            object.current_game_data = list([])

            generate_guess = object.generate_hidden_sequence()
            object.generated_guess = generate_guess
            print("Generated Guess", object.generated_guess)

    def convert_to_string(self, string_from) -> list():
        return list(map(int, string_from))


class MinecraftInput:

    def minecraft_input_handle(self, inputstring, gridsize, helpmessage):
        cell = ()
        flag = False
        message = "Invalid cell. " + helpmessage

        pattern = r'([a-{}])([0-9]+)(f?)'.format(ascii_lowercase[gridsize - 1])
        validinput = re.match(pattern, inputstring)

        if inputstring == 'help':
            message = helpmessage
   
        elif inputstring == 'exit':
            message = 'exit'

        elif inputstring == 'exit':
            message = 'exit'

        elif validinput:
            row_number = int(validinput.group(2)) - 1
            column_number = ascii_lowercase.index(validinput.group(1))
            flag = bool(validinput.group(3))

            if -1 < row_number < gridsize:
                cell = (row_number, column_number)
                message = ''

        return {'cell': cell, 'flag': flag, 'message': message}


class BlackjackInput:

    def handle_input(self, input: str):
        if input == "hit" or input == "stand":
            return input
        else:
            return "invalid input, please hit or stand"
